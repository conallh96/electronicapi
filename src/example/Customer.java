package example;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;


@Entity
public class Customer {


	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	private String name, email ,address1, address2, county, eircode, phoneNo, type, cardNo;
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	List<OrderSheet> orders = new ArrayList<OrderSheet>();
	
	
	public Customer() {
		
	}


	public Customer(String name, String email, String address1, String address2, String county, String eircode,
			String phoneNo, String cardNo, String type) {
		super();
		this.name = name;
		this.email = email;
		this.address1 = address1;
		this.address2 = address2;
		this.county = county;
		this.eircode = eircode;
		this.phoneNo = phoneNo;
		this.cardNo = cardNo;
		this.type = type;
	
	}
	
	public void addOrder(OrderSheet order) {
		this.orders.add(order);
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address) {
		this.address1 = address;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}




	public String getAddress2() {
		return address2;
	}



	public void setAddress2(String address2) {
		this.address2 = address2;
	}



	public String getCounty() {
		return county;
	}



	public void setCounty(String county) {
		this.county = county;
	}



	public String getEircode() {
		return eircode;
	}



	public void setEircode(String eircode) {
		this.eircode = eircode;
	}



	public String getType() {
		return type;
	}



	public void setType(String type) {
		this.type = type;
	}


	public List<OrderSheet> getOrders() {
		return orders;
	}


	public void setOrders(List<OrderSheet> orders) {
		this.orders = orders;
	}


	public String getCardNo() {
		return cardNo;
	}


	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}
	
	



}

