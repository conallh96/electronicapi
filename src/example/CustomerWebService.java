package example;


import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;


@Path ("/customerwebservice")
public class CustomerWebService {
	
	CustomerDAO dao = new CustomerDAO();
	
	@GET
	@Path ("/customers")
	@Produces(MediaType.TEXT_HTML)
	public String getCustomers() {
		CustomerDAO dao = new CustomerDAO();
		String allItems = "";
		for (Customer cust : dao.getAllCustomers()) {
			
			Gson gson = new Gson();
			String custString = gson.toJson(cust);
			
			if(allItems.equals("")){
			
			allItems = allItems + custString;
			
			}
			else {
				
				allItems = allItems + "/" + custString;
				
			}
	
		}
		return  allItems;
	}
	
	
	
	@DELETE
	@Path("/removeId/{id}")
	public String deleteById(@PathParam("id") int id) {
		
		dao.removeCustomer(dao.getById(id));
		return "Removed Customer with id: " + id;
	}
	
	
	
	@DELETE
	@Path("/remove/{name}")
	public String deleteByName(@PathParam("name") String name) {
		
		dao.removeCustomer(dao.getCustomerByName(name));
		return "Removed Customer with name: " + name;
	}
	
	
	@GET
	@Path("/customerId/{id}")
	@Produces({MediaType.APPLICATION_JSON})
	public Customer getCustomerById(@PathParam("id")int id) {
		
		return dao.getById(id);
	}
	
	@GET
	@Path("/customerEmail/{email}")
	@Produces({MediaType.APPLICATION_JSON})
	public Customer getCustomerByEmail(@PathParam("email")String email) {
		
		return dao.getByEmail(email);
	}
	
	
	@POST
	@Path("/post")
	@Consumes("application/json")
	@Produces({MediaType.APPLICATION_JSON})
	public Customer createUtil(Customer cust) {
		
		dao.persistCustomer(cust);
		return cust;
	}
	
	@PUT
	@Path("/update/{id}")
	@Consumes("application/json")
	@Produces({MediaType.APPLICATION_JSON})
	public Customer updateUtil( Customer cust,@PathParam("id") int id) {
		
		cust.setId(id);
		dao.mergeCustomer(cust);
		return  cust;
	}

	

}