package example;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class ReviewDAO {
	
	protected static EntityManagerFactory emf =
			Persistence.createEntityManagerFactory("ConallsPU");
			

	public ReviewDAO() {
		
	}
	
	public void persistReview(Review review) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		em.persist(review);
		em.getTransaction().commit();
		em.close();
	}
	
	public void mergeReview(Review review) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		em.merge(review);
		em.getTransaction().commit();
		em.close();
	}
	
	public void removeReview(Review review) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		em.remove(em.merge(review));
		em.getTransaction().commit();
		em.close();
	}

	public List<Review> getAllReviews(){
		
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		List<Review> reviews = new ArrayList<Review>();
		reviews = em.createQuery("from Review").getResultList();
		em.getTransaction().commit();
		em.close();
		
		return reviews;
	}
	

	
	
	public Review getById(int id) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		Review review = em.createQuery("SELECT r FROM Review r WHERE r.id = :id", Review.class)
                .setParameter("id", id)
                .getSingleResult();
		em.getTransaction().commit();
		em.close();
		return review;
	}
	
	public List<Review> getByProductId(int productId) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		List<Review> reviews = new ArrayList<Review>();
		reviews = em.createQuery("from Review r Where r.productId = :productId").setParameter("productId",productId)
				.getResultList();
		
		em.getTransaction().commit();
		em.close();
		return reviews;
	}
	
	
	public List<Review> getByCustomerId(int customerId) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		List<Review> reviews = new ArrayList<Review>();
		reviews = em.createQuery("from Review").getResultList();
		for (Review r : reviews) {
			if(!(r.getCustomerId()==customerId)) {
				reviews.remove(r);
			}
		}
		em.getTransaction().commit();
		em.close();
		return reviews;
	}


}
