package example;


import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;


@Path ("/reviewwebservice")
public class ReviewWebService {
	
	ReviewDAO dao = new ReviewDAO();
	
	@GET
	@Path ("/reviews")
	@Produces(MediaType.TEXT_HTML)
	public String getReviews() {
		
		String allItems = "";
		for (Review review : dao.getAllReviews()) {
			
			Gson gson = new Gson();
			String revString = gson.toJson(review);
			
			if(allItems.equals("")){
			
			allItems = allItems + revString;
			
			}
			else {
				
				allItems = allItems + "/" + revString;
				
			}
	
		}
		return  allItems;
	}
	
	
	
	@DELETE
	@Path("/removeId/{id}")
	public String deleteById(@PathParam("id") int id) {
		
		dao.removeReview(dao.getById(id));
		return "Removed Review with id: " + id;
	}
	
	

	@GET
	@Path("/productId/{id}")
	@Produces({MediaType.APPLICATION_JSON})
	public String getReviewsProductById(@PathParam("id")int id) {
		
		String allItems = "";
		for (Review review : dao.getByProductId(id)) {
			
			Gson gson = new Gson();
			String revString = gson.toJson(review);
			
			if(allItems.equals("")){
			
			allItems = allItems + revString;
			
			}
			else {
				
				allItems = allItems + "/" + revString;
				
			}
	
		}
		
	return  allItems;
	}
	

	@GET
	@Path("/customerId/{id}")
	@Produces({MediaType.APPLICATION_JSON})
	public String getReviewsByCustomerId(@PathParam("id")int id) {
		
		String allItems = "";
		for (Review review : dao.getByCustomerId(id)) {
			
			Gson gson = new Gson();
			String revString = gson.toJson(review);
			
			if(allItems.equals("")){
			
			allItems = allItems + revString;
			
			}
			else {
				
				allItems = allItems + "/" + revString;
				
			}
	
		}
		
	return  allItems;
	}

	
	@POST
	@Path("/post")
	@Consumes("application/json")
	@Produces({MediaType.APPLICATION_JSON})
	public Review createReview(Review review) {
		
		dao.persistReview(review);
		return review;
	}
	
	@PUT
	@Path("/update/{id}")
	@Consumes("application/json")
	@Produces({MediaType.APPLICATION_JSON})
	public Review updateReview( Review review ,@PathParam("id") int id) {
		
		review.setId(id);
		dao.mergeReview(review);
		return  review;
	}

	

}