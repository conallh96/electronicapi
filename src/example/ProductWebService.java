package example;


import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;


@Path ("/productwebservice")
public class ProductWebService {
	
	ProductDAO dao = new ProductDAO();
	
	@GET
	@Path ("/products")
	@Produces(MediaType.TEXT_HTML)
	public String getProducts() {
		ProductDAO dao = new ProductDAO();
		String allItems = "";
		for (Product prod : dao.getAllProducts()) {
			
			Gson gson = new Gson();
			String prodString = gson.toJson(prod);
			
			if(allItems.equals("")){
			
			allItems = allItems + prodString;
			
			}
			else {
				
				allItems = allItems + "/" + prodString;
				
			}
	
		}
		return  allItems;
	}
	
	
	
	@DELETE
	@Path("/removeId/{id}")
	public String deleteById(@PathParam("id") int id) {
		
		dao.removeProduct(dao.getById(id));
		return "Removed Product with id: " + id;
	}
	
	
	
	@DELETE
	@Path("/remove/{name}")
	public String deleteByName(@PathParam("name") String name) {
		
		dao.removeProduct(dao.getProductByName(name));
		return "Removed Product with name: " + name;
	}
	
	
	@GET
	@Path("/productId/{id}")
	@Produces({MediaType.APPLICATION_JSON})
	public Product getProductById(@PathParam("id")int id) {
		
		return dao.getById(id);
	}
	
	
	@POST
	@Path("/post")
	@Consumes("application/json")
	@Produces({MediaType.APPLICATION_JSON})
	public Product createProduct(Product product) {
		
		dao.persistProduct(product);
		return product;
	}
	
	@PUT
	@Path("/update/{id}")
	@Consumes("application/json")
	@Produces({MediaType.APPLICATION_JSON})
	public Product updateProduct( Product prod,@PathParam("id") int id) {
		
		prod.setId(id);
		dao.mergeProduct(prod);
		return  prod;
	}

	

}