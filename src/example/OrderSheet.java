package example;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

@Entity
public class OrderSheet {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	private int customerId;
	private double total;
    @ManyToMany(fetch = FetchType.EAGER)
	List<Product> products = new ArrayList<Product>();

	public OrderSheet() {
		// TODO Auto-generated constructor stub
	}



	public OrderSheet(int customerId, List<Product> products) {
		super();
		
		this.customerId = customerId;
		this.products = products;
		
		double cost = 0.0;
		for (Product product : products) {
			
			cost = cost + product.getPrice();
			
		}
		this.total=cost;
	}



	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	
	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}
	
	
	

}
