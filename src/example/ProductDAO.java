package example;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class ProductDAO {
	
	protected static EntityManagerFactory emf =
			Persistence.createEntityManagerFactory("ConallsPU");
			

	public ProductDAO() {
		
	}
	
	public void persistProduct(Product product) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		em.persist(product);
		em.getTransaction().commit();
		em.close();
	}
	
	public void mergeProduct(Product product) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		em.merge(product);
		em.getTransaction().commit();
		em.close();
	}
	
	public void removeProduct(Product product) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		em.remove(em.merge(product));
		em.getTransaction().commit();
		em.close();
	}
	
	public void buyProduct(Product product) {
		Product buying = getById(product.getId());
		buying.setStock((buying.getStock()-1));
		
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
	
		em.merge(buying);

		em.getTransaction().commit();
		em.close();
		
		
	}

	public List<Product> getAllProducts(){
		
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		List<Product> products = new ArrayList<Product>();
		products = em.createQuery("from Product").getResultList();
		em.getTransaction().commit();
		em.close();
		
		return products;
	}
	
	
	public void persistOrder(OrderSheet order) {
		
		CustomerDAO cDao = new CustomerDAO();
		Customer c = cDao.getById(order.getCustomerId());
	
		HashMap<Integer, Integer> stockReduction = new HashMap<>();
		for (Product p : order.getProducts()) {
		Integer count = stockReduction.get(p.getId());
		if (count == null) {
			stockReduction.put(p.getId(), 1);
		}
		else {
			stockReduction.put(p.getId(), count + 1);
		}
		}
	
		for (Map.Entry<Integer, Integer> entry : stockReduction.entrySet()) {
		    int key = entry.getKey();
		    int value = entry.getValue();
		    
		    for (Product p: order.getProducts()) {
		    	if(p.getId()==key) {
		    	p.setStock(p.getStock()-value);
		    	mergeProduct(p);
		    
		    }
		}
		}
		
		
		c.addOrder(order);
		cDao.mergeCustomer(c);
		
	}

	
	public Product getById(int id) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		Product product = em.createQuery("SELECT p FROM Product p WHERE p.id = :id", Product.class)
                .setParameter("id", id)
                .getSingleResult();
		em.getTransaction().commit();
		em.close();
		return product;
	}
	
	
	public Product getProductByName(String name) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		Product product = em.createQuery("SELECT p FROM Product p WHERE p.name = :name", Product.class)
                .setParameter("name", name)
                .getSingleResult();
		em.getTransaction().commit();
		em.close();
		return product;
	}
	
	


}
