package example;

import java.awt.Image;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;

public class TestDatabase {

	public TestDatabase() {
		
		ProductDAO pdao = new ProductDAO();
		CustomerDAO cdao = new CustomerDAO();
		OrderDAO odao = new OrderDAO();
		
		
		Customer customer1 = new Customer("Conall Hunt", "conallhuntbray@gmail.com", "Park Lodge, Lower Dargle Road",
				"Bray", "Wicklow", "A98 K032", "0860271811", "43219123456789012", "Customer");
		
		cdao.persistCustomer(customer1);
		
		Product iPadPro = null;
		

   
        File file = new File("/Users/conallhunt/Downloads/ipad-pro.jpg");
        byte[] bFile = new byte[(int) file.length()];
 
        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            fileInputStream.read(bFile);
            fileInputStream.close();
            iPadPro = new Product("iPad Pro 256GB", "Apple", "Tablets", bFile, 1099.99, 15);
        } catch (Exception e) {
            e.printStackTrace();
        }
		
			
		pdao.persistProduct(iPadPro);
		
		
		Product macbookAir =null;

        File file2 = new File("/Users/conallhunt/Downloads/macbook-air.jpg");
        byte[] bFile2 = new byte[(int) file2.length()];
 
        try {
            FileInputStream fileInputStream = new FileInputStream(file2);
            fileInputStream.read(bFile2);
            fileInputStream.close();
            macbookAir = new Product("MacBook Air", "Apple", "Notebooks", bFile2, 1399.99, 20);
        } catch (Exception e) {
            e.printStackTrace();
        }
		    
		
		pdao.persistProduct(macbookAir);
		
		ArrayList<Product> products = new ArrayList<Product>();
		products.add(pdao.getProductByName(macbookAir.getName()));
		products.add(pdao.getProductByName(iPadPro.getName()));
		
		Customer fromDb = cdao.getCustomerByName(customer1.getName());
		
		OrderSheet order = new OrderSheet(fromDb.getId(), products);
		
		ArrayList<OrderSheet> orders = new ArrayList<OrderSheet>();
		orders.add(order);		
		fromDb.setOrders(orders);
		
		cdao.mergeCustomer(fromDb);
		
		
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new TestDatabase();
	}

}
