package example;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class OrderDAO {
	
	protected static EntityManagerFactory emf =
			Persistence.createEntityManagerFactory("ConallsPU");
			

	public OrderDAO() {
		
	}
	
	public void persistOrder(OrderSheet order) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		em.persist(order);
		em.getTransaction().commit();
		em.close();
	}
	
	public void mergeOrder(OrderSheet order) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		em.merge(order);
		em.getTransaction().commit();
		em.close();
	}
	
	public void removeOrder(OrderSheet order) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		em.remove(em.merge(order));
		em.getTransaction().commit();
		em.close();
	}

	public List<OrderSheet> getAllOrders(){
		
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		List<OrderSheet> orders = new ArrayList<OrderSheet>();
		orders = em.createQuery("from Ordersheet").getResultList();
		em.getTransaction().commit();
		em.close();
		
		return orders;
	}
	
	public List<OrderSheet> getAllCustomersOrders(int id){
		
		CustomerDAO cDao = new CustomerDAO();
		
		
		
		return cDao.getById(id).getOrders();
	}
	
	

	
	public OrderSheet getById(int id) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		OrderSheet order = em.createQuery("SELECT o FROM OrderSheet o WHERE o.id = :id", OrderSheet.class)
                .setParameter("id", id)
                .getSingleResult();
		em.getTransaction().commit();
		em.close();
		return order;
	}
	

	
	


}
