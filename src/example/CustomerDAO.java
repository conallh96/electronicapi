package example;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class CustomerDAO {
	
	protected static EntityManagerFactory emf =
			Persistence.createEntityManagerFactory("ConallsPU");
			

	public CustomerDAO() {
		
	}
	
	public void persistCustomer(Customer customer) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		em.persist(customer);
		em.getTransaction().commit();
		em.close();
	}
	
	public void mergeCustomer(Customer customer) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		em.merge(customer);
		em.getTransaction().commit();
		em.close();
	}
	
	public void removeCustomer(Customer customer) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		em.remove(em.merge(customer));
		em.getTransaction().commit();
		em.close();
	}

	public List<Customer> getAllCustomers(){
		
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		List<Customer> users = new ArrayList<Customer>();
		users = em.createQuery("from Customer").getResultList();
		em.getTransaction().commit();
		em.close();
		
		return users;
	}
	
	
	
	public Customer getCustomerByName(String name) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		Customer customer = em.createQuery("SELECT c FROM Customer c WHERE c.name = :name", Customer.class)
                .setParameter("name", name)
                .getSingleResult();
		em.getTransaction().commit();
		em.close();
		return customer;
	}
	
	
	public Customer getById(int id) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		Customer customer = em.createQuery("SELECT c FROM Customer c WHERE c.id = :id", Customer.class)
                .setParameter("id", id)
                .getSingleResult();
		em.getTransaction().commit();
		em.close();
		return customer;
	}
	
	
	public Customer getByEmail(String email) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		Customer customer = em.createQuery("SELECT c FROM Customer c WHERE c.email = :email", Customer.class)
                .setParameter("email", email)
                .getSingleResult();
		em.getTransaction().commit();
		em.close();
		return customer;
	}

}
