package example;


import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;


@Path ("/orderwebservice")
public class OrderWebService {
	
	ProductDAO pDao = new ProductDAO();
	OrderDAO dao = new OrderDAO();
	
	@GET
	@Path ("/orders")
	@Produces(MediaType.TEXT_HTML)
	public String getOrders() {
		
		String allItems = "";
		for (OrderSheet order : dao.getAllOrders()) {
			
			Gson gson = new Gson();
			String orderString = gson.toJson(order);
			
			if(allItems.equals("")){
			
			allItems = allItems + orderString;
			
			}
			else {
				
				allItems = allItems + "/" + orderString;
				
			}
	
		}
		return  allItems;
	}
	
	
	
	@DELETE
	@Path("/removeId/{id}")
	public String deleteById(@PathParam("id") int id) {
		
		dao.removeOrder(dao.getById(id));
		return "Removed Product with id: " + id;
	}
	
	
	@GET
	@Path("/customerId/{id}")
	@Produces({MediaType.APPLICATION_JSON})
	public String getOrdersByCustomerId(@PathParam("id")int id) {
		
		String allItems = "";
		for (OrderSheet order : dao.getAllCustomersOrders(id)) {
			
			Gson gson = new Gson();
			String ordString = gson.toJson(order);
			
			if(allItems.equals("")){
			
			allItems = allItems + ordString;
			
			}
			else {
				
				allItems = allItems + "/" + ordString;
				
			}
	
		}
		
	return  allItems;
	}

	
	
	@GET
	@Path("/orderId/{id}")
	@Produces({MediaType.APPLICATION_JSON})
	public OrderSheet getOrderById(@PathParam("id")int id) {
		
		return dao.getById(id);
	}
	
	
	@POST
	@Path("/post")
	@Consumes("application/json")
	@Produces({MediaType.APPLICATION_JSON})
	public OrderSheet createOrder(OrderSheet order) {
	
		pDao.persistOrder(order);
		return order;
	}
	


	

}